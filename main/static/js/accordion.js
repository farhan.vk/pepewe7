$(document).ready(function(){
    $(".title").click(function(){
      if ($(this).parent().hasClass("active")){
          $(this).parent().removeClass("active");
      } else {
          $(".acc-header").removeClass("active")
          $(this).parent().addClass("active");
      }            
    });
    $(".move-up").click(function(){
      var $item = $(this).parent().parent().parent();
      $item.prev().before($item);
    });
    $(".move-down").click(function(){
      var $item = $(this).parent().parent().parent();
      $item.next().after($item);
    });           
});